#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, redirect, jsonify, render_template, url_for, request
from flask.ext.login import LoginManager, login_user, logout_user, login_required, current_user
from database_adapter import DatabaseAdapter
from users_provider import User

app = Flask(__name__)
# ключ для шифра сессии
app.config['SECRET_KEY'] = """MIICXAIBAAKBgQCdxBVziJPtI00nUK2OZnFZfrHJdsFGHBSRda8O7z5zT5nspSip
vGtea8qM37dj37NgNy7zvS/+sz/wI8oaDTeEBs+kYdUUC7IAjLOl+5V0Pl7/H++/
H5awWSWnKWKd9eInr8Z3JEPsh7qPM0fkCfyrj3iO9ydUoJ22jQVzU+ZjGQIDAQAB
AoGBAIC78RkeOD7HkHxlO8m31ARH+oC/M4SnAy2Sju8rz5S+Poa4Wg7u3dnlMRl0
r6uCbVc0kE55cMFvuNuWCKLraAE9tW3zPqZFGUi6Pm1n+2H6IACUMSuBj+z3hEhV
wH//8cfzD/zOHKm72AYfJghayiRANl9JC0H/Z3Hqp8oc80I1AkEA2NCc+zO7eYBx
m+91COLHAGn8pOjQVCudWGAI7RXJqI0ulDVn+ylBbsW1BLO18mwNuf7eznDsEYA4
+ZMxMqsIOwJBALpHcyo7t9N6O3Nril1J5FlG9O6glf/4EjJYlAFd6mldw0Dlk4Ef
V1+WKKHXjSVJg3NRTAP+Krw2X1NlccDQILsCQBMqNYYa54pw6Rnwi7bSXObTd4qY"""

login_manager = LoginManager()
login_manager.init_app(app)
db_adapter = DatabaseAdapter()


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/admin', methods=['get'])
# @login_required Значит что только авторизированые пользователи
# имеют доступ
@login_required
def admin():
    return render_template("admin.html")


@app.route('/check', methods=['get'])
@login_required
def check_auth():
    return jsonify(auth=True)


@app.route('/login', methods=['post'])
def login_check():
    # получаем json из запроса
    jsonData = request.get_json()
    # исчем юзера
    user = User.get(jsonData['username'])
    # проверяем пароль
    if user and user.password == jsonData['password']:
        login_user(user)
    else:
        return jsonify(error="Wrong login or password")
    return jsonify(success="True")


@app.route('/consoles', methods=['get'])
def get_consoles():
    print("Get consoles...")
    return jsonify(consoles=db_adapter.get_all_consoles())


@app.route('/reserve', methods=['post'])
def reserve():
    json_data = request.get_json()
    # проводим валидацию значений
    if not json_data["dateTime"]:
        return jsonify(error="Неверная дата!")
    if "consoleId" not in json_data or not json_data["consoleId"] or int(json_data["consoleId"]) == 0:
        return jsonify(error="Выберите консоль!")
    if "playersCount" not in json_data or not json_data["playersCount"] or not 1 <= int(json_data["playersCount"]) <= 4:
        return jsonify(error="Неверное количество игроков!")
    # формируем объект записи
    record = {
        "client_name": json_data["clientName"],
        "date": json_data["dateTime"].split()[0],
        "time": json_data["dateTime"].split()[1],
        "console_id": int(json_data["consoleId"]),
        "players": int(json_data["playersCount"])
    }
    # сохраняем в базу
    insert_result = db_adapter.insert_reserve_record(record)
    if not isinstance(insert_result, int) or insert_result == -1:
        return jsonify(error=insert_result)
    else:
        return jsonify(success="True")


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/timetable', methods=['get'])
@login_required
def get_records():
    return jsonify(records=db_adapter.get_all_records())


@app.route('/timetable', methods=['put'])
@login_required
def edit_record():
    json_data = request.get_json() 
    if "Id" not in json_data or not json_data["Id"] or json_data["Id"] == "-1":
        return jsonify(error="Запись не найдена!")
    if not json_data["dateTime"]:
        return jsonify(error="Неверная дата!")
    if not json_data["clientName"]:
        return jsonify(error="Введите имя игрока!")
    if "consoleId" not in json_data or not json_data["consoleId"] or int(json_data["consoleId"]) == 0:
        return jsonify(error="Выберите консоль!")
    if "playersCount" not in json_data or not json_data["playersCount"] or not 1 <= int(json_data["playersCount"]) <= 4:
        return jsonify(error="Неверное количество игроков!")
    record = {
        "client_name": json_data["clientName"],
        "date": json_data["dateTime"].split()[0],
        "time": json_data["dateTime"].split()[1],
        "console_id": int(json_data["consoleId"]),
        "players": int(json_data["playersCount"]),
        "id": int(json_data["Id"]),
        "user_id": current_user.get_id()
    }
    insert_result = db_adapter.update_reserve_record(record)
    if not isinstance(insert_result, int) or not insert_result:
        return jsonify(error=insert_result)
    else:
        return jsonify(success="True")


@app.route('/timetable', methods=['post'])
@login_required
def add_record():
    json_data = request.get_json()
    print(json_data)
    if not json_data["dateTime"]:
        return jsonify(error="Неверная дата!")
    if not json_data["clientName"]:
        return jsonify(error="Введите имя игрока!")
    if "consoleId" not in json_data or not json_data["consoleId"] or int(json_data["consoleId"]) == 0:
        return jsonify(error="Выберите консоль!")
    if "playersCount" not in json_data or not json_data["playersCount"] or not 1 <= int(json_data["playersCount"]) <= 4:
        return jsonify(error="Неверное количество игроков!")
    record = {
        "client_name": json_data["clientName"],
        "date": json_data["dateTime"].split()[0],
        "time": json_data["dateTime"].split()[1],
        "console_id": int(json_data["consoleId"]),
        "players": int(json_data["playersCount"])
    }
    insert_result = db_adapter.insert_reserve_record(record)
    if not isinstance(insert_result, int) or insert_result == -1:
        return jsonify(error=insert_result)
    else:
        return jsonify(success="True", id=insert_result)


@app.route('/timetable', methods=['delete'])
@login_required
def delete_record():
    json_data = request.get_json()
    print(json_data)
    if "Id" not in json_data or not json_data["Id"] or json_data["Id"] == "-1":
        return jsonify(error="Запись не найдена!")

    delete_result = db_adapter.delete_reserve_record(int(json_data["Id"]))
    if not isinstance(delete_result, int) or not delete_result:
        return jsonify(error=delete_result)
    else:
        return jsonify(success="True")


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)

if __name__ == '__main__':
    app.run(debug=True,
           threaded=True)