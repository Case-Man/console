function onLoadFunction(){
    $("#order_form").trigger('reset');
    loadConsoles();
    loadRecords();

    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true
    });
}

var currentRecordId = -1;
var clickedRow = null;

function onRowClick(row){
    // обработка нажатия на строку таблицы
    // внесения данных со строки в форму редактирования
    clickedRow = row;
    currentRecordId = row.getElementsByTagName("td")[0].innerHTML;
    var clientName = row.getElementsByTagName("td")[1].innerHTML;
    var date = row.getElementsByTagName("td")[2].innerHTML;
    var dateParts = date.split('-');
    var time = row.getElementsByTagName("td")[3].innerHTML;
    var timeParts = time.split(':');
    var consoleId = row.getElementsByTagName("td")[4].innerHTML;
    var playersCount = row.getElementsByTagName("td")[5].innerHTML;
    $('*[name=date16]').handleDtpicker('setDate', new Date(parseInt(dateParts[0]), parseInt(dateParts[1]) - 1, parseInt(dateParts[2]),
        parseInt(timeParts[0]), parseInt(timeParts[1]), 0));
    $("#name_input").val(clientName);
    $("#players_count").val(parseInt(playersCount));
    $("#console_select").val(consoleId).change();
    $("html, body").animate({ scrollTop: 0 }, "slow");
}

// обрабработка нажатия на кнопку удалить
function deleteRecordMessage(){
    $("#delete_popup_link").trigger('click');
}

// удаление записи
function deleteRecord(){
    dataObject = {
        "Id": currentRecordId
    }
    $.ajax({
            type: "DELETE",
            url: "/timetable",
            contentType: 'application/json',
            dataType : 'json',
            data: JSON.stringify(dataObject),
            success : function(result) {
                if(result["error"]){
                    $("#fail_popup_link").trigger('click');
                    $("#fail_popup_label").html("Ошибка: " + result["error"]);
                }
                else {
                    // удаляем строку из таблицы
                    clickedRow.remove();
                    $("#success_popup_link").trigger('click');
                    $("#order_form").trigger('reset');
                }
            },
            error : function(result){
                $("#fail_popup_link").trigger('click');
                $("#fail_popup_label").html("Ошибка: " + result["error"]);
            }})
}

// функция для загрузки списка консолей
function loadConsoles(){
    $.ajax({
            type: "GET",
            url: "/consoles",
            contentType: 'application/json',
            dataType : 'json',
            success : function(result) {
                if(result["error"]){
                    $("#fail_popup_link").trigger('click');
                    $("#fail_popup_label").html("Ошибка: " + result["error"]);
                }
                else {
                    $(result["consoles"]).map(function () {
                        var option = $('<option />');
                        option.attr('value', this.id).text(this.name);
                        $('#console_select').append(option);
                    });
                }
            },
            error : function(result){
                $("#fail_popup_link").trigger('click');
                $("#fail_popup_label").html("Ошибка: " + result["error"]);
            }})
}

// форматируем хтмл для вставки строки таблицы
function formatRowHtml(rId, client_name,date,time,console_id,players_count){
    var deleteSection = "<a class='button-link_delete' style='' onclick='deleteRecordMessage()'>Удалить</a>";
    return "<tr onclick=\"onRowClick(this)\"><td>" + rId + "</td><td>" + client_name +
               "</td><td>" + date +"</td><td>" + time + "</td><td>" + console_id +
               "</td><td>" + players_count + "</td><td>" + deleteSection + "</td></tr>";
}

// загрузка данных в таблицу брони
function loadRecords(){
    $.ajax({
            type: "GET",
            url: "/timetable",
            contentType: 'application/json',
            dataType : 'json',
            success : function(result) {
                if(result["records"]){
                    // добавляем строки
                    $(result["records"]).map(function () {
                        $('#orders_table > tbody').append(formatRowHtml(this.id, this.client_name, this.date, this.time, this.console_id, this.players_count));
                    });
                }
            },
            error : function(result){
                // сообщение об ошибке
                $("#fail_popup_link").trigger('click');
                $("#fail_popup_label").html("Ошибка: " + result["error"]);
            }})
}

// сохранение заказа
function saveSubmit(form) {
    console.log(form);
    // получаем данные с формы
    dataObject = {
        "dateTime": form.date16.value,
        "consoleId": form.console.value,
        "clientName": form.clientName.value,
        "playersCount": form.playersCount.value,
        "Id": currentRecordId
    }

    $.ajax({
            // выбираем метод в зависимости редактирование или добавление нового
            type: ((currentRecordId != -1) ? "PUT" : "POST"),
            url: "/timetable",
            contentType: 'application/json',
            dataType : 'json',
            data: JSON.stringify(dataObject),
            success : function(result) {
                if(result["error"]){
                    // сообщение об ошибке
                    $("#fail_popup_link").trigger('click');
                    $("#fail_popup_label").html("Ошибка: " + result["error"]);
                }
                else {
                    if(currentRecordId != -1){
                        // если редактирование изменяем данные в таблице
                        clickedRow.getElementsByTagName("td")[1].innerHTML = dataObject.clientName;
                        clickedRow.getElementsByTagName("td")[2].innerHTML =dataObject.dateTime.split(' ')[0];
                        clickedRow.getElementsByTagName("td")[3].innerHTML = dataObject.dateTime.split(' ')[1];
                        clickedRow.getElementsByTagName("td")[4].innerHTML = dataObject.consoleId;
                        clickedRow.getElementsByTagName("td")[5].innerHTML = dataObject.playersCount;
                    }
                    else{
                        // если добавление - добавляем новою строку в таблицу
                        $('#orders_table > tbody').append(formatRowHtml(result["id"], dataObject.clientName,
                            dataObject.dateTime.split(' ')[0], dataObject.dateTime.split(' ')[1],
                            dataObject.consoleId, dataObject.playersCount));
                    }
                    $("#success_popup_link").trigger('click');
                    $("#order_form").trigger('reset');
                }
            },
            error : function(result){
                $("#fail_popup_link").trigger('click');
                $("#fail_popup_label").html("Ошибка: " + result["error"]);
            }})
}
