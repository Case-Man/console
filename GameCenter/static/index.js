function onLoadFunction(){
    $("#order_form").trigger('reset');
    loadConsoles();
    checkAuth();
}

// функция для загрузки списка консолей
function loadConsoles(){
    $.ajax({
            type: "GET",
            url: "/consoles",
            contentType: 'application/json',
            dataType : 'json',
            success : function(result) {
                if(result["error"]){
                    // в случаи ошибки показываем сообщение
                    $("#fail_popup_link").trigger('click');
                    $("#fail_popup_label").html("Ошибка: " + result["error"]);
                }
                else {
                    // в случаи удач загружаем в select данные
                    $(result["consoles"]).map(function () {
                        var option = $('<option />');
                        option.attr('value', this.id).text(this.name);
                        $('#console_select').append(option);
                    });
                }
            },
            error : function(result){
                $("#fail_popup_link").trigger('click');
                $("#fail_popup_label").html("Ошибка: " + result["error"]);
            }})
}

// функция для проверки авторизации пользователя
// чтоб отображать или не отображать кнопку входа
function checkAuth(){
    $.ajax({
            type: "GET",
            url: "/check",
            contentType: 'application/json',
            dataType : 'json',
            success : function(result) {
                // авторизирован
                $("#login_popup_link").hide();
                $("#admin_link").show();
            },
            error : function(result){
                // не авторизирован
                $("#admin_link").hide();
                $("#login_popup_link").show();
            }})
}

$(document).ready(function() {
    // инициализация всплывающих окон
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true
    });
});

// логин
function loginSubmit(form){
    dataObject = {
        "username": form.username.value,
        "password": calcMD5(form.password.value)
    }
    $.ajax({
            type: "POST",
            url: "/login",
            contentType: 'application/json',
            dataType : 'json',
            data: JSON.stringify(dataObject),
            success : function(result) {
                if(result["error"]){
                    $("#fail_popup_link").trigger('click');
                    $("#fail_popup_label").html("Ошибка: " + result["error"]);
                }
                else
                    location.href = "/admin";
            },
            error : function(result){
                console.log(result);
            }})
}

// резервирование времени
function reserveSubmit(form){
    dataObject = {
        "dateTime": form.date16.value,
        "consoleId": form.console.value,
        "clientName": form.clientName.value,
        "playersCount": form.playersCount.value
    }
    $.ajax({
            type: "POST",
            url: "/reserve",
            contentType: 'application/json',
            dataType : 'json',
            data: JSON.stringify(dataObject),
            success : function(result) {
                if(result["error"]){
                    $("#fail_popup_link").trigger('click');
                    $("#fail_popup_label").html("Ошибка: " + result["error"]);
                }
                else {
                    $("#success_popup_link").trigger('click');
                    $("#order_form").trigger('reset');
                }
            },
            error : function(result){
                $("#fail_popup_link").trigger('click');
                $("#fail_popup_label").html("Ошибка: " + result["error"]);
            }})
}