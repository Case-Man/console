#!/usr/bin/python
# -*- coding: utf-8 -*-

import mysql.connector


class DatabaseAdapter:
    def __init__(self):
        # читаем конфигурацию подключения к базе с файла
        f = open("config.ini")
        self.connect_config = eval(" ".join([line for line in f]))
        f.close()

    # получение списка пользователей
    def get_all_users(self):
        records = {}
        # создаем подключение
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        statement = 'SELECT * FROM users '
        cursor.execute(statement)
        # проходим по сету из базы
        for (id, login, password) in cursor:
            record = {
                "login": login,
                "password": password
            }
            # добавляем в dictionary, где ключ - логин
            records[record['login']] = record
        cursor.close()
        con.close()
        return records

    # получение списка всех консолей
    # алгоритм работы как выше
    def get_all_consoles(self):
        consoles = []
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        statement = 'SELECT * FROM consoles '
        cursor.execute(statement)
        for (id, name) in cursor:
            consoles.append({
                "id": id,
                "name": name
            })
        cursor.close()
        con.close()
        return consoles

    # проверка свободна ли консоль в то время
    def check_time(self, date, time, console_id, id=0):
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        statement = "SELECT COUNT(*) FROM timetable where Date = '%s' and Time = '%s' and ConsoleId = %d" % \
                    (date, time, console_id)
        if id:
            # если передан id пользователя в случаи редактирования заказа
            statement += " AND Id <> " + str(id)
        cursor.execute(statement)
        for (count) in cursor:
            if count[0] == 0:
                return True
        return False

    # добавление бронирования
    def insert_reserve_record(self, record):
        # проверяем время
        if not self.check_time(record["date"], record["time"], int(record["console_id"]),
                               record["id"] if "id" in record else 0):
            return "Время занято!"
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        add_record_statement = ("""INSERT INTO timetable (ClientName, Date, Time, ConsoleId, PlayersCount)
                                    VALUES ('%(client_name)s', '%(date)s', '%(time)s', %(console_id)d, %(players)d)""" % record)
        cursor.execute(add_record_statement)
        con.commit()
        last_row_id = cursor.lastrowid
        cursor.close()
        con.close()
        return last_row_id

    # редактирование бронирования
    def update_reserve_record(self, record):
        # проверяем время
        if not self.check_time(record["date"], record["time"], int(record["console_id"]),
                               record["id"] if "id" in record else 0):
            return "Время занято!"
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        get_user_id_statement = "SELECT Id FROM Users where Login = '" + record["user_id"] + "';"
        cursor.execute(get_user_id_statement)
        for id in cursor:
            record["edited_by"] = id[0]
        update_record_statement = ("""UPDATE timetable SET ClientName = '%(client_name)s', Date = '%(date)s',
                                   Time = '%(time)s', ConsoleId = %(console_id)d, PlayersCount = %(players)d, EditedBy = %(edited_by)d
                                   WHERE Id = %(id)d""" % record)
        cursor.execute(update_record_statement)
        con.commit()
        cursor.close()
        con.close()
        return cursor.rowcount

    # удаление бронирования
    def delete_reserve_record(self, id):
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        update_record_statement = ("""DELETE FROM timetable  WHERE Id = %d""" % id)
        cursor.execute(update_record_statement)
        con.commit()
        cursor.close()
        con.close()
        return cursor.rowcount

    # получение спрска заказов
    def get_all_records(self):
        records = []
        con = mysql.connector.connect(**self.connect_config)
        cursor = con.cursor()
        statement = 'SELECT * FROM timetable '
        cursor.execute(statement)
        for (record_id, client_name, date, time, console_id, payers_count, edited_by) in cursor:
            record = {
                "id": record_id,
                "client_name": client_name,
                "date": date,
                "time": time,
                "console_id": console_id,
                "players_count": payers_count,
                "edited_by": edited_by
            }
            records.append(record)
        cursor.close()
        con.close()
        return records