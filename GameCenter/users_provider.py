from flask.ext.login import UserMixin
from database_adapter import DatabaseAdapter

data_adapter = DatabaseAdapter()


class UserNotFoundError(Exception):
    pass


class User(UserMixin):
    def __init__(self, ID):
        all_users = data_adapter.get_all_users()
        if not ID in all_users:
            raise UserNotFoundError()
        self.id = ID
        self.password = all_users[ID]['password']

    @classmethod
    def get(self_class, id):
        try:
            return self_class(id)
        except UserNotFoundError:
            return None
